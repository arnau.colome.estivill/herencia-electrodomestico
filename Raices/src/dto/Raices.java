package dto;

public class Raices {
	
	//Atributos Raices
	private double a;
	private double b;
	private double c;
	
	//Constructor por defecto
	public Raices() {
		this.a = 0.0;
		this.b = 0.0;
		this.c = 0.0;
	}

	//Constructor con los atributos
	public Raices(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	@Override
	public String toString() {
		return "Raices [a=" + a + ", b=" + b + ", c=" + c + "]";
	}
	
	//imprime las 2 posibles soluciones
	public void obtenerRaices(double a, double b, double c) {	
		double aRaiz = Math.sqrt(a);
		double bRaiz = Math.sqrt(b);
		double cRaiz = Math.sqrt(c);
		
		if(aRaiz>0) {
			System.out.println("La raiz de 'a' es igual a " + aRaiz + " y " + -aRaiz);				
		}else {
			System.out.println("La raiz de 'a' es igual a " + aRaiz);				
		}
		
		if(bRaiz>0) {
			System.out.println("La raiz de 'b' es igual a " + bRaiz + " y " + -bRaiz);				
		}else {
			System.out.println("La raiz de 'b' es igual a " + bRaiz);				
		}

		if(cRaiz>0) {
			System.out.println("La raiz de 'c' es igual a " + cRaiz + " y " + -cRaiz);				
		}else {
			System.out.println("La raiz de 'c' es igual a " + cRaiz);				
		}
		
	}
	
	//imprime única raíz, que será cuando solo tenga una solución posible
	public void obtenerRaiz(double a, double b, double c) {	
		double aRaiz = Math.sqrt(a);
		double bRaiz = Math.sqrt(b);
		double cRaiz = Math.sqrt(c);
		
		System.out.println("La raiz de 'a' es igual a " + aRaiz);				
		System.out.println("La raiz de 'b' es igual a " + bRaiz);				
		System.out.println("La raiz de 'c' es igual a " + cRaiz);
	}

	/*devuelve el valor del discriminante (double), el discriminante
	tiene la siguiente formula, (b^2)-4*a*c*/
	public double getDiscriminante(double a, double b, double c) {		
		double resultado = (Math.pow(b, 2) -4*a*c);						
		return resultado;
	}
	
	/*devuelve un booleano indicando si tiene dos soluciones, para que
	esto ocurra, el discriminante debe ser mayor o igual que 0*/
	public boolean tieneRaices(double a, double b, double c) {	
		boolean resultado = false;
		double discrimianante = getDiscriminante(a, b, c);
		
		if(discrimianante >= 0) {
			resultado = true;
		}	
		
		return resultado;		
	}
	
	/*devuelve un booleano indicando si tiene una única solución, para que
	esto ocurra, el discriminante debe ser igual que 0*/
	public boolean tieneRaiz(double a, double b, double c) {	
		boolean resultado = false;
		double discrimianante = getDiscriminante(a, b, c);
		
		if(discrimianante == 0) {
			resultado = true;
		}		
		 
		return resultado;	
	}
	
	/*mostrara por consola las posibles soluciones que tiene nuestra ecuación,
	en caso de no existir solución, mostrarlo también*/
	public void calcular(double a, double b, double c) {
		//Formula ecuación 2º grado: (-b±√((b^2)-(4*a*c)))/(2*a)
		double operacionX1 = (-b + Math.sqrt(Math.pow(b, 2) -4*a*c)) / (2*a);
		double operacionX2 = (-b - Math.sqrt(Math.pow(b, 2) -4*a*c)) / (2*a); 
		
		System.out.println("La 'x1' es igual a " + operacionX1 + " y la 'x2' es igual a " + operacionX2);		
	}

}
