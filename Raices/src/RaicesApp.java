import dto.Raices;

import javax.swing.JOptionPane;

public class RaicesApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Preguntamos al usuario el atributo 'a'
		String a = JOptionPane.showInputDialog("Introduce el valor de 'a'");
		//Convertimos el String a Double
		Double aDouble = Double.parseDouble(a);
		
		//Preguntamos al usuario el atributo 'b'
		String b = JOptionPane.showInputDialog("Introduce el valor de 'b'");
		//Convertimos el String a Double
		Double bDouble = Double.parseDouble(b);
		
		//Preguntamos al usuario el atributo 'c'
		String c = JOptionPane.showInputDialog("Introduce el valor de 'c'");
		//Convertimos el String a Double
		Double cDouble = Double.parseDouble(c);
		
		//Crear el objeto Raices
		Raices r = new Raices(aDouble, bDouble, cDouble);
		
		//Array de las operaciones, para el objecto seleccion
		String[] operacion = {"obtenerRaices", "obtenerRaiz", "getDiscriminante", "tieneRaices", "tieneRaiz", "calcular"};
		//Preguntamos al usuario que operacion realizarc
		Object seleccion = JOptionPane.showInputDialog(null, "Que operacion desea hacer:", "Seleccion",  JOptionPane.DEFAULT_OPTION, null, operacion, "0");
		
		//Con el switch llamamos a un metodo dependiendo de la operacion selecionada
		switch (seleccion.toString()) {
			case "obtenerRaices":
				r.obtenerRaices(aDouble, bDouble, cDouble);
				break;
				
			case "obtenerRaiz":
				r.obtenerRaiz(aDouble, bDouble, cDouble);				
				break;	
				
			case "getDiscriminante":
				double resultadoDouble = r.getDiscriminante(aDouble, bDouble, cDouble);
				System.out.println(resultadoDouble);
				break;	
				
			case "tieneRaices":
				boolean tieneRaices = r.tieneRaices(aDouble, bDouble, cDouble);		
				System.out.println(tieneRaices);
				break;
				
			case "tieneRaiz":
				boolean tieneRaiz = r.tieneRaiz(aDouble, bDouble, cDouble);
				System.out.println(tieneRaiz);
				break;
				
			case "calcular":
				r.calcular(aDouble, bDouble, cDouble);
				break;
		}
				
	}

}
