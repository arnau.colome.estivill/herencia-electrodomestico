package dto;

public class Libro {
	//Atributos
	
	private String isbn;
	private String titulo;
	private String autor;
	private int paginas;
	
	//Constructor con valores introducidos por teclado para poder generar los objetos en el main
	public Libro(String isbn, String titulo, String autor, int paginas) {
		super();
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.paginas = paginas;
	}
	
	//M�todos get y set de todos los atributos.
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public int getPaginas() {
		return paginas;
	}
	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}
	
	/*M�todo toString() para mostrar la informaci�n relativa al libro con el formato:
	 * "El libro con ISBN creado por el autor tiene p�ginas"
	 */
	
	@Override
	public String toString() {
		return "El libro " + titulo + " con ISBN " + isbn + " creado por el autor " + autor + " tiene " + paginas + " p�ginas.";
	}
	
	
	
}
