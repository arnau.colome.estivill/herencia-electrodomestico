import javax.swing.JOptionPane;
import dto.Libro;

public class LibroApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int pag [] = new int [2] ; 
		for (int i = 0; i <= 1; i++) {
			String isbn = JOptionPane.showInputDialog("Introduce el ISBN del libro: ");	
			String titulo = JOptionPane.showInputDialog("Introduce el titulo del libro: ");		
			String autor = JOptionPane.showInputDialog("Introduce el autor del libro: ");		
			String paginas = JOptionPane.showInputDialog("Introduce el n�mero de p�ginas del libro: ");
			int paginasInt = Integer.parseInt(paginas);
			pag [i] = paginasInt;
			
			Libro l1 = new Libro(isbn, titulo, autor, paginasInt);
			
			System.out.println(l1);
				
		}
		if (pag[0]>pag[1]) {
			System.out.println("El primer libro tiene m�s p�ginas!");
		}else if (pag[0]<pag[1]) {
			System.out.println("El segundo libro tiene m�s p�ginas!");
		}else {
			System.out.println("Tienen las mismas p�ginas!");
		}
	}

}
