
package dto;

public class Pelicula {
	//Atributos
	private String titulo;
	private int duracion;
	private int edadMinima;
	private String director;
	
	//Constructor por defect
	public Pelicula() {
		this.titulo = "";
		this.duracion = 0;
		this.edadMinima = 0;
		this.director = "";
	}
	
	//Constructor con todos los atributos por teclado
	public Pelicula(String titulo, int duracion, int edadMinima, String director) {
		this.titulo = titulo;
		this.duracion = duracion;
		this.edadMinima = edadMinima;
		this.director = director;
	}
	
	//Getters de los atributos
	public String getTitulo() {
		return titulo;
	}
	public int getDuracion() {
		return duracion;
	}
	public int getEdadMinima() {
		return edadMinima;
	}
	public String getDirector() {
		return director;
	}
	
	//Setters de los atributos
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	
	
	//M�todo toString
	@Override
	public String toString() {
		return  titulo + " con una duraci�n de " + duracion + " minutos, para p�blico de " + edadMinima + " a�os y del director "
				+ director + ".";
	}
	
	
}
