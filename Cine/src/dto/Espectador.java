package dto;

public class Espectador {
	//Atributos
	private String nombre;
	private int edad;
	private double dinero;
	
	public Espectador() {
		this.nombre = "";
		this.edad = 0;
		this.dinero = 0;
	}
	
	//Constructor con todos los atributos por teclado
	public Espectador(String nombre, int edad, double dinero) {
		this.nombre = nombre;
		this.edad = edad;
		this.dinero = dinero;
	}
	
	//Getters de los atributos
	public String getNombre() {
		return nombre;
	}
	public int getEdad() {
		return edad;
	}
	public double getDinero() {
		return dinero;
	}
	
	//Setters de los atributos
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public void setDinero(double dinero) {
		this.dinero = dinero;
	}
	
	//M�todo toString
	@Override
	public String toString() {
		return "Espectador [nombre=" + nombre + ", edad=" + edad + ", dinero=" + dinero + "]";
	}
		
}
