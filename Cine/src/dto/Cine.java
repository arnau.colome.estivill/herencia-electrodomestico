package dto;


public class Cine {
	//Atributos
	
	private Pelicula peli;
	private double precio;
	
	//Constructor con todos los atributos por teclado
	public Cine(Pelicula peli, double precio) {
		this.peli = peli;
		this.precio = precio;
	}

	//Getters de los atributos
	public Pelicula getPeli() {
		return peli;
	}
	public double getPrecio() {
		return precio;
	}

	//Setters de los atributos
	public void setPeli(Pelicula peli) {
		this.peli = peli;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	//M�todo toString
	@Override
	public String toString() {
		return "Cine [peli=" + peli + ", precio=" + precio + "]";
	}
	
	//Generem la sala (8 files i 9 columnes)
	public String[][] sala() {
		String sala[][] = new String [8][9]; 
		String lletres []  = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j<=8; j++) {
				sala[i][j] = 8-i + lletres[j];
			}
		}
		return sala;
	}
	public boolean salaLlena (String [][]sala) {
		boolean  llena = true;
		String [] lletres = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j<=8; j++) {
				if (sala[i][j].equals(8-i + lletres[j])){
					llena = false;
				}
			}		
		}
		
		return llena;
	}
	

	
	
}
