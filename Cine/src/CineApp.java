import dto.Espectador;
import dto.Pelicula;
import dto.Cine;

import java.util.Random;

public class CineApp {

	public static void main(String[] args) {
		//Generamos las listas de los atributos de la clase espectador. Estos ser�n elegidos aleat�riamente.
		String[] nombre = {"Alba", "Jaume","N�dia","Arnau","Jordi","Anna","Daniel","Pol", "Esther", "Raquel"};
		int[] edad = {10,20,30,13,15,5,55,60,40,19};
		double[] dinero = {3,4.5,3.2,2,5,6.5, 4,1.8,6,7};
		
		//Generamos la pel�cula, el cine i un objeto random
		Pelicula peli = new Pelicula("El corredor del laberinto", 113, 12,"Wes Ball");
		Cine cine = new Cine (peli, 3);
		Random random = new Random();
		
		//Definimos variables i generamos la clase
		String [] lletres = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
		boolean ocupado = true;
		String [][] sala = cine.sala();
		
		/*Generamos muchos esectadores y los sentamos aleatoriamente(no podemos donde ya este ocupado).
		 * Solo se podr�n sentar si tienen el suficiente dinero, hay espacio libre y tiene edad para ver la pel�cula, en caso de que el asiento
		 * este ocupado le buscamos uno libre. 
		 */
		System.out.println("Bienvenidos al cine! La pel�cula de hoy es " + peli);
		for (int i = 0; i < 150; i++) {
			Espectador espectador = new Espectador(nombre[random.nextInt(10)], edad[random.nextInt(10)], dinero[random.nextInt(10)]);
			System.out.println(espectador);
			
			//En caso de que a sala este llena, el programa finaliza.
			if (cine.salaLlena(sala)==true) {
				System.out.println("La sala est� llena! Vuelve en otro momento, gracias.");
				break;	
			}
			
			if (espectador.getDinero()>= cine.getPrecio()) {
				if (espectador.getEdad() >= peli.getEdadMinima()) {
				
					do {
						String lloc = sala[random.nextInt(8)][random.nextInt(9)];
						if (lloc.equals("ocupado")) {
							System.out.println("Este asiento esta ocupado, ahora te asignamos otro.");
							ocupado = true;
						}
						for (int k = 0; k < 8; k++) {
							for (int j = 0; j<=8; j++) {
								
								if (lloc.equals(8-k + lletres[j])) {
									ocupado = false;
									System.out.println("Tu asiento es: " + lloc );
									sala[k][j] = "ocupado";
								}
							}
						}
								
					}while(ocupado == true);
				
				}else {
					System.out.println("Lo sentimos pero no le esta permitido el acceso porque no tiene la edad m�nima.");
				}		
			}else {
				System.out.println("Lo sentimos pero no tiene suficiente dinero.");
			}
			System.out.println(" ");
		
		}
		
	}

}
