package dto;

public class Profesor extends Persona {

	//variables
	private String materia;
	
	//herencia DANIEL
	public Profesor() {		
		super();
		this.materia = "";
	}
	
	public Profesor(String nombre, int edad, boolean sexo, String materia) {
		super(nombre, edad, sexo, true);
		this.materia = materia;
	}

	public String getMateria() {
		return materia;
	}
		
}
