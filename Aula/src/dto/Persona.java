package dto;

public class Persona {

	//esta clase es para generar alumnos y profesores
	
	//primero crearemos las variables
	
	protected String nombre;
	protected int edad;
	protected char sexo;
	protected boolean asistencia;
	
	public Persona() {
		this.nombre = "";
		this.edad = 0;
		this.sexo = Character.MIN_VALUE; //H o M
		this.asistencia = false;
	}
	
	public Persona(String nombre, int edad, boolean sexo, boolean tipo) {
		this.nombre = nombre;
		this.edad = edad;
		//El valor boolean de sexo viene de un random que lo pasemos a 'H' o 'M' dependiendo de si es true o false
		this.sexo = (sexo == true) ? 'H' : 'M';
		this.asistencia = (tipo == true) ? asistencia(true) : asistencia();
	}
	
	//Se aplica el polimorfismo si se introduce un valor boolean como parametro
	public boolean asistencia(boolean x) {
		double random = Math.random();
		//random < 0.8 sirve para modificar la probabilidad
		boolean randomBoolean = random < 0.8;
				
		return randomBoolean;
	}
	
	public boolean asistencia() {
		double random = Math.random();
		//random < 0.5 sirve para modificar la probabilidad
		boolean randomBoolean = random < 0.5;
		
		return randomBoolean;
	}

	public boolean isAsistencia() {
		return asistencia;
	}
	
}
