package dto;

public class Estudiante extends Persona {

	private double calificacion;
	
	public Estudiante() {
		super();
		this.calificacion = 0.0;
	}
	
	public Estudiante(String nombre, int edad, boolean sexo, double calificacion) {
		super(nombre, edad, sexo, false);
		this.calificacion = calificacion;
	}
		
}
