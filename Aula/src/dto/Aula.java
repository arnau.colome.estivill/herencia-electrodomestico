package dto;

public class Aula {
	
	/*
	
	aula con estudiante y profesor

	estudiantes (nombre, edad, sexo) + calificación actual
	profesor (nombre, edad, sexo) + materia impartida 

	materias (nmatemáticas, filosofía, física)
	
	estudiantes 50% de hacer novillos (faltar) pero aunque falte el sitio queda
	
	profesor 20% de no encontrarse disponible (reuniones, baja, ..) 
	
	las dos se deben llamar igual (polimorfismo)
	
	aula tiene identifiicador numerico, numero max estudiantes y asignatura (alguno mas)
	
	para dar clase necesita: profesor disponible, clase profesor y +50% alumnos
	
	determinar si se puede dar clase
	
	mostrar numero alumnos y alumnas aprovados
	
	*/
	
	private int id;
	private int maxEstudiantes;
	private String materia;
	private boolean profesorEstado;
	private String profesorMateria;
	private int totalAsistenciaEstudiantes;
	
	public Aula() {
		this.id = 0;
		this.maxEstudiantes = 0;
		this.materia = "";
		this.profesorEstado = false;
		this.profesorMateria = "";
		this.totalAsistenciaEstudiantes = 0;
	}
	
	public Aula(int id, int maxEstudiantes, String materia, boolean profesorEstado, String profesorMateria, int totalAsistenciaEstudiantes) {
		this.id = id;
		this.maxEstudiantes = maxEstudiantes;
		this.materia = materia;
		this.profesorEstado = profesorEstado;
		this.profesorMateria = profesorMateria;
		this.totalAsistenciaEstudiantes = totalAsistenciaEstudiantes;
	}

	public String getMateria() {
		return materia;
	}

	public boolean isProfesorEstado() {
		return profesorEstado;
	}

	public String getProfesorMateria() {
		return profesorMateria;
	}
	

	@Override
	public String toString() {
		
		if(sePuedeDarClase() == true) {
			return "Se puede dar clase";
		}else {
			return "No se puede dar clase";
		}
		
	}

	public boolean sePuedeDarClase(){
		boolean resultado = false;
		
		if(this.isProfesorEstado() == true && this.getMateria().equals(this.getProfesorMateria()) && this.totalAsistenciaEstudiantes > (this.totalAsistenciaEstudiantes / 2)) {
			resultado = true;
		}
		
		return resultado;
	}
	
}
