import java.util.Hashtable;
import java.util.Random;

import dto.Aula;
import dto.Profesor;
import dto.Estudiante;

public class AulaApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Definir parametros
		int totalDeClases = 10;
		int totalDeAlumnosClase = 5;
		
		//Crear arrays para objectos
		Aula[] aulas = new Aula[totalDeClases];
		Profesor[] profesores = new Profesor[totalDeClases];
		Estudiante[][] estudiantes = new Estudiante[totalDeClases][totalDeAlumnosClase];
		
		//Crear hashtable para almacenar informacio y relacionarla con la aula
		Hashtable<Integer, String> sePuedeDarClase = new Hashtable<>();
		Hashtable<Integer, Integer> hashtableEstudiantesAprobadosH = new Hashtable<>();
		Hashtable<Integer, Integer> hashtableEstudiantesAprobadosM = new Hashtable<>();
		
		//Crear string como recurso para los objetos Profesor y Estudiante
		String[] nombres = {"Antonio", "Jose", "David", "Laura", "Marta"};
		String[] materias = {"Matem�ticas", "Filosof�a", "F�sica"};
		
		for (int i = 0; i < totalDeClases; i++) {
			//Obtener un nombre aleatorio del array nombres
			Random nombreIndiceProfesor = new Random();
			int nombreIndiceProfesorInt = nombreIndiceProfesor.nextInt(4 + 1 - 0) + 0;
			
			//Obtener una edad aleatoria
			Random edadProfesor = new Random();
			int edadProfesorInt = edadProfesor.nextInt(65 + 1 - 16) + 16;
			
			//Obtener un sexo
			Random sexoProfesor = new Random();
			boolean sexoProfesorBoolean = sexoProfesor.nextBoolean();
							
			//Obtener una materia aleatorio del array materias
			Random materiaIndiceProfesor = new Random();
			int materiaIndiceProfesoresInt = materiaIndiceProfesor.nextInt(2 + 1 - 0) + 0;
			
			//Instanciar objeto
			profesores[i] = new Profesor(nombres[nombreIndiceProfesorInt], edadProfesorInt, sexoProfesorBoolean, materias[materiaIndiceProfesoresInt]);
			
			//Crear arrays para almacenar informacion temporal
			int totalAsistenciaEstudiantes = 0;
			int estudiantesAprobadosH = 0;
			int estudiantesAprobadosM = 0;
		
			for (int j = 0; j < totalDeAlumnosClase; j++) {
			
				//Obtener un nombre aleatorio del array nombres
				Random nombreIndiceEstudiante = new Random();
				int nombreEstudianteInt = nombreIndiceEstudiante.nextInt(4 + 1 - 0) + 0;
				
				//Obtener una edad aleatoria
				Random edadEstudiante = new Random();
				int edadEstudianteInt = edadEstudiante.nextInt(50 + 1 - 3) + 3;
				
				//Obtener un sexo
				Random sexoEstudiante = new Random();
				boolean sexoEstudianteBoolean = sexoEstudiante.nextBoolean();
				
				//Obtener una calificacion aleatoria
				Random calificacionEstudiante = new Random();
				int calificacionEstudianteInt = calificacionEstudiante.nextInt(10 + 1 - 0) + 0;
								
				//Si esta aprobado y depende del sexo va a un array o a otro
				if(calificacionEstudianteInt > 5 && sexoEstudianteBoolean == true) {
					estudiantesAprobadosH += 1;
				}else if(calificacionEstudianteInt > 5 && sexoEstudianteBoolean == false) {
					estudiantesAprobadosM += 1;
				} 
				
				//Instanciamos el objeto
				estudiantes[i][j] = new Estudiante(nombres[nombreEstudianteInt], edadEstudianteInt, sexoEstudianteBoolean, calificacionEstudianteInt);
				
				//Almacenamos la asistencia de una clase
				totalAsistenciaEstudiantes += (estudiantes[i][j].isAsistencia() == true) ? 1 : 0;

			}
			
			//Obtenemos un id aleatorio
			Random idAula = new Random();
			int idAulaInt = idAula.nextInt(100 + 1 - 0) + 0;
			
			//Obtener una materia aleatoria del array
			Random materiaAula = new Random();
			int materiaAulaInt = materiaAula.nextInt(2 + 1 - 0) + 0;
			
			//Instanciamos el objeto
			aulas[i] = new Aula(idAulaInt, totalDeAlumnosClase, materias[materiaAulaInt], profesores[i].isAsistencia(), profesores[i].getMateria(), totalAsistenciaEstudiantes);
			
			//Si se puede dar esa clase mostarmos el n� de aprobados
			if(aulas[i].sePuedeDarClase()) {
				hashtableEstudiantesAprobadosH.put(idAulaInt, estudiantesAprobadosH);
				hashtableEstudiantesAprobadosM.put(idAulaInt, estudiantesAprobadosM);	
			}
			
			//Almacenamos si se puede dar clase
			sePuedeDarClase.put(idAulaInt, aulas[i].toString());
		}
		
		
		//Mostramos en general si se puede dar clase
		System.out.println("General: " + sePuedeDarClase);
		//Mostramos los alumnos aprobados por clase
		System.out.println("Alumnos aprobados por clase: " + hashtableEstudiantesAprobadosH);
		//Mostramos los alumnas aprobados por clase
		System.out.println("Alumnas aprobador por clase: " + hashtableEstudiantesAprobadosM);
	}

}
