import dto.Serie;
import dto.Videojuego;

public class SerieApp {

	public static void main(String[] args) {

	/*
		
	 	Implementa los anteriores m�todos en las clases Videojuego y Serie. 
	 	Ahora crea una aplicaci�n ejecutable y realiza lo siguiente:
	 	
		Crea dos arrays, uno de Series y otro de Videojuegos, de 5 posiciones cada uno.
		
		Crea un objeto en cada posici�n del array, con los valores que desees, puedes usar distintos constructores.
		
		Entrega algunos Videojuegos y Series con el m�todo entregar().
		
		Cuenta cuantos Series y Videojuegos hay entregados. Al contarlos, devu�lvelos.
		
		Por �ltimo, indica el Videojuego tiene m�s horas estimadas y la serie con mas temporadas. 
		Muestralos en pantalla con toda su informaci�n (usa el m�todo toString()).
	 
	*/
		
		//Crea dos arrays, uno de Series y otro de Videojuegos, de 5 posiciones cada uno.
		Serie[] aSerie = new Serie[5];
		Videojuego[] aVideojuego = new Videojuego [5];
		
		//Crea un objeto en cada posici�n del array, con los valores que desees, puedes usar distintos constructores.
		aSerie[0]=new Serie("Peppa Pig", 6, "infantil", "Carton network");
		aSerie[1]=new Serie("Vikings", 2, "historia", "Netflix");
		aSerie[2]=new Serie("Lucifer", 4, "Demonios", "Netflix");
		aSerie[3]=new Serie("hawai 5.0", 10, "Policiaca", "Hawaii");
		aSerie[4]=new Serie("The Blacklist", 7, "Policiaca", "Netflix");
		
		aVideojuego[0]=new Videojuego("The last of us", 80, "Historia", "Naughty dog");
		aVideojuego[1]=new Videojuego("Fortnite", 4, "rpg", "Epic games");
		aVideojuego[2]=new Videojuego("Minecraft", 200, "mundo abierto", "Mojang");
		aVideojuego[3]=new Videojuego("Grand theft auto V", 20, "rpg", "Rockstar");
		aVideojuego[4]=new Videojuego("Pokemon Go", 50, "mundo abierto", "Niantic");		
		
		//Entrega algunos Videojuegos y Series con el m�todo entregar().
		aSerie[2].entregar();
		aSerie[3].entregar();
		aSerie[4].entregar();
		aVideojuego[1].entregar();
		
		//Cuenta cuantos Series y Videojuegos hay entregados. Al contarlos, devu�lvelos.
		int entregado=0;
		//usamos un bucle de 5 (tama�o del array)para contar los entregados
		
		for (int i = 0; i < 5; i++) {
			if (aSerie[i].isEntregado()) {
				entregado++;
				aSerie[i].devolver();		
			}
			if(aVideojuego[i].isEntregado()) {
				entregado++;
				aVideojuego[i].devolver();
			}
		}
		
		
		// Por �ltimo, indica el Videojuego tiene m�s horas estimadas y la serie con mas temporadas. 
		//tenemos que hacer un array para guardarlo
		Serie sLarga = aSerie[0];
		Videojuego vLargo = aVideojuego[0];
		
		for (int o=0; o < 5; o++) {
			if (aSerie[o].compareTo(sLarga) == "Tiene mas temporadas") {
				sLarga = aSerie[o];
			}
			
			if (aVideojuego[o].compareTo(vLargo) == "Tiene mas horas") {
				vLargo = aVideojuego[o];
			}
			
		}
		
		// Muestralos en pantalla con toda su informaci�n (usa el m�todo toString()).
		
		System.out.println("Numero series y videojuegos entregados: " + entregado);
		System.out.println("Serie mas larga: " + sLarga);
		System.out.println("Videojuego mas largo: " + vLargo);

	}

}
