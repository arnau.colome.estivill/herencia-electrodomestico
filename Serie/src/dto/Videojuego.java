package dto;

public class Videojuego implements Entregable  {
	//Atributos
	
	protected String titulo;
	protected int horasEstimadas;
	protected boolean entregado;
	protected String genero;
	protected String compania;

	//Constructor por defecto
	public Videojuego() {
		this.titulo = "";
		this.horasEstimadas = 10;
		this.entregado = false;
		this.genero = "";
		this.compania = "";
	}

	//Constructor con el titulo y horas estimadas introducidas por teclado. El resto por defecto
	public Videojuego(String titulo, int horasEstimadas) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
	}
	
	//Constructor con todos los atributos, excepto de entregado	
	public Videojuego(String titulo, int horasEstimadas, String genero, String compania) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.genero = genero;
		this.compania = compania;
	}

	
	//Getters de todos los atributos excepto entregado.
	public String getTitulo() {
		return titulo;
	}
	public int getHorasEstimadas() {
		return horasEstimadas;
	}
	public String getCompania() {
		return compania;
	}
	public String getGenero() {
		return genero;
	}
	
	//Setters de todos los atributos excepto entregado.
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public void setCompania(String compania) {
		this.compania = compania;
	}

	
	//Implementamos los m�todos del entregable
	public void entregar() {
		this.entregado = true;
	}
	
	public void devolver() {
		this.entregado = false;
	}
	
	public boolean isEntregado() {
		if(entregado) {
			return true;
		}
		return false;
	}
		
	public String compareTo(Object a) {	
		Videojuego vid = (Videojuego) a;
		if (this.horasEstimadas >= vid.getHorasEstimadas()) {
			return "Tiene mas horas";	
		}
		else {
			return "Tiene menos horas";
		}
		
 	}
	
	//M�todo toString
	@Override
	public String toString() {
		return "[titulo=" + titulo + ", horasEstimadas=" + horasEstimadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", compania=" + compania + "]";
	}
	
	
	
}
