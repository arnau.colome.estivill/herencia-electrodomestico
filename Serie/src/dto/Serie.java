package dto;

public class Serie implements Entregable{
	
	/*
		Crearemos una clase llamada Serie con las siguientes características:
		• Sus atributos son titulo, numero de temporadas, entregado, genero y creador.
		• Por defecto, el numero de temporadas es de 3 temporadas y entregado false.
		El resto de atributos serán valores por defecto según el tipo del atributo.
		Los constructores que se implementaran serán:
		✓ Un constructor por defecto.
		✓ Un constructor con el titulo y creador. El resto por defecto.
		✓ Un constructor con todos los atributos, excepto de entregado.
		Los métodos que se implementara serán:
		• Métodos get de todos los atributos, excepto de entregado.
		• Métodos set de todos los atributos, excepto de entregado.
		• Sobrescribe los métodos toString.	
	*/
	
	//Sus atributos son titulo, numero de temporadas, entregado, genero y creador.
	protected String titulo;
	protected int nTemporadas;
	protected boolean entregado;
	protected String genero;
	protected String creador;
	
	
	//Por defecto, el numero de temporadas es de 3 temporadas y entregado false.
	// ✓ Un constructor por defecto.
	// ✓ Un constructor con el titulo y creador. El resto por defecto.	
	// ✓ Un constructor con todos los atributos, excepto de entregado.
	public Serie() {
		this.titulo="";
		this.nTemporadas=3;
		this.entregado=false;
		this.genero="";
		this.creador="";
	}

	public Serie(String titulo, String creador) {
		this.titulo = titulo;
		this.creador = creador;
	}

	public Serie(String titulo, int nTemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.nTemporadas = nTemporadas;
		this.genero = genero;
		this.creador = creador;
	}

	/*
		Los métodos que se implementara serán:
		• Métodos get de todos los atributos, excepto de entregado.
		• Métodos set de todos los atributos, excepto de entregado.
		• Sobrescribe los métodos toString.	
	 */

	public String getTitulo() {
		return titulo;
	}

	public int getnTemporadas() {
		return nTemporadas;
	}

	public String getGenero() {
		return genero;
	}

	public String getCreador() {
		return creador;
	}

	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setnTemporadas(int nTemporadas) {
		this.nTemporadas = nTemporadas;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	
	public void entregar() {
		entregado=true;
	}
	public void devolver() {
		entregado=false;
	}
	
	public boolean isEntregado() {
		if(entregado) {
			return true;
		}
		return false;
	}
	
	public String compareTo(Object a) {	
		Serie ser = (Serie) a;
		if (this.nTemporadas >= ser.getnTemporadas()) {
			return "Tiene mas temporadas";	
		}
		else {
			return "Tiene menos temporadas";
		}
		
 	}
	
	
	@Override
	public String toString() {
		return "[titulo=" + titulo + ", nTemporadas=" + nTemporadas + ", entregado=" + entregado + ", genero="
				+ genero + ", creador=" + creador + "]";
	}
	

	
}
