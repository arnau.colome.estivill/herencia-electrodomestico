package dto;

public class Lavadora extends Electrodomestico{
	/*
	Crearemos una subclase llamada Lavadora con las siguientes características:
	• Su atributo es carga, ademas de los atributos heredados.
	• Por defecto, la carga es de 5 kg. Usa una constante para ello.
	
	Los constructores que se implementaran serán:
	• Un constructor por defecto.
	• Un constructor con el precio y peso. El resto por defecto.
	• Un constructor con la carga y el resto de atributos heredados. Recuerda que debes llamar al constructor de la clase padre.
	
	Los métodos que se implementara serán:
	• Método get de carga.
	• precioFinal():, si tiene una carga mayor de 30 kg, aumentara el precio 50	€, sino es así no se incrementara el precio. 
	Llama al método padre y	añade el código necesario. 
	Recuerda que las condiciones que hemos visto en la clase Electrodomestico también deben afectar al precio. 
	*/
	
	// Por defecto, la carga es de 5 kg. Usa una constante para ello.
	final double pes=5;
	
	// Su atributo es carga, ademas de los atributos heredados.
	protected double carga;

	//Un constructor por defecto.
	public Lavadora() {
		this.carga = pes;
	}

	// Un constructor con el precio y peso. El resto por defecto.
	public Lavadora(double precio, double peso) {	
		super(precio, peso);
		this.precioBase = precio;
		this.peso = peso;
	}
	//Un constructor con la carga y el resto de atributos heredados. Recuerda que debes llamar al constructor de la clase padre.
	public Lavadora(double carga, double precioBase, String color, char eficienciaEnergetica, double pesoDouble) {

		super(precioBase, color, eficienciaEnergetica, pesoDouble);
		this.precioBase = precioFinal(precioBase);
		this.peso = pesoDouble;
	}
	
	// Método get de carga.
	public double getCarga() {
		return carga;
	}

	/* precioFinal():, si tiene una carga mayor de 30 kg, aumentara el precio 50€, sino es así no se incrementara el precio. 
		Llama al método padre y añade el código necesario. 
		Recuerda que las condiciones que hemos visto en la clase Electrodomestico también deben afectar al precio. 
	*/
	
	public double precioFinal(double precioBase) {
		if (carga >= 30){
			precioBase = precioBase + 50;
		}
		return precioBase;
	}

	@Override
	public String toString() {
		return "Lavadora [carga=" + carga + ", precioBase=" + precioBase + ", color=" + color
				+ ", eficienciaEnergetica=" + eficienciaEnergetica + ", peso=" + peso + "]";
	}
	
}
