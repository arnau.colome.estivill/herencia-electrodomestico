package dto;

public class Television extends Electrodomestico{
	
	
	// Introducimos los valores por defecto
	final int RESOLUCION_DEF = 20;
	final boolean SINTONIZADOR_TDT_DEF = false;
	
	// Introducimos los atributos
	protected int resolucion; 
	protected boolean sintonizadorTdt;

	//Constructor por defecto
	public Television() {
		super();
		this.resolucion = RESOLUCION_DEF;
		this.sintonizadorTdt = SINTONIZADOR_TDT_DEF;
	}

	//Constructor con el precio y el peso. El resto por defecto.
	public Television(double precio, double peso) {
		super(precio, peso);
		this.precioBase = precio;
		this.peso = peso;
	}
	
	//Constructor con la resolucion, el sintonizador TDT y el resto heredados.
		public Television(int resolucion, boolean sintonizadorTdt,double precioBase, String color,char eficienciaEnergetica, double pesoDouble) {
			super(precioBase,color,eficienciaEnergetica,pesoDouble);
			this.precioBase = precioFinal();
		}
	
	//M�todo get de resoluci�n.
	public int getResolucion() {
		return resolucion;
	}

	//M�todo get del sintonizador.
	public boolean isSintonizadorTdt() {
		return sintonizadorTdt;
	}

	/*Funcion precio final, si tiene una resoluci�n mayor de 40 pulgadas incrementa el precio un 30% y
	 * si tiene un sintonizador TDT aumentara 50�.
	 */
	
	public double precioFinal() {
		double precioFin = this.precioBase;
		
		if (resolucion > 40) {
			precioFin = precioFin + precioFin*0.30;
		}
		
		if (sintonizadorTdt == true) {
			precioFin = precioFin + 50;
		}

		return precioFin;
	}

	@Override
	public String toString() {
		return "Television [resolucion=" + resolucion + ", sintonizadorTdt=" + sintonizadorTdt + ", precioBase="
				+ precioBase + ", color=" + color + ", eficienciaEnergetica=" + eficienciaEnergetica + ", peso=" + peso
				+ "]";
	}	
	
}
