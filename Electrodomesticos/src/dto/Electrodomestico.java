package dto;

public class Electrodomestico {
	
	final double PRECIO_BASE_DEFAULT = 100;
	final String COLOR_DEFAULT = "BLANCO";
	final char CONSUMO_ENERGETICO_DEFAULT = 'F';
	final double PESO_DEFAULT = 5.0;
	
	protected double precioBase;
	protected String color;
	protected char eficienciaEnergetica;
	protected double peso;
	
	public Electrodomestico() {
		this.precioBase = PRECIO_BASE_DEFAULT;
		this.color = COLOR_DEFAULT;
		this.eficienciaEnergetica = CONSUMO_ENERGETICO_DEFAULT;
		this.peso = PESO_DEFAULT;
	}
	
	public Electrodomestico(double precio, double peso) {	
		this.precioBase = precio;
		this.color = COLOR_DEFAULT;
		this.eficienciaEnergetica = CONSUMO_ENERGETICO_DEFAULT;
		this.peso = peso;
	}
	
	public Electrodomestico(double precioBase, String color, char eficienciaEnergetica, double peso) {
		
		color = (comprobarColor(color) == true) ? color : COLOR_DEFAULT;
		eficienciaEnergetica = (comprobareficienciaEnergetica(eficienciaEnergetica) == true) ? eficienciaEnergetica : CONSUMO_ENERGETICO_DEFAULT;		
		precioBase = precioFinal(eficienciaEnergetica, peso, precioBase);
		
		this.precioBase = precioBase;
		this.color = color;
		this.eficienciaEnergetica = eficienciaEnergetica;		
		this.peso = peso;
	}

	public double getPrecioBase() {
		return precioBase;
	}

	public String getColor() {
		return color;
	}

	public char geteficienciaEnergetica() {
		return eficienciaEnergetica;
	}

	public double getPeso() {
		return peso;
	}
	
	public boolean comprobareficienciaEnergetica(char eficienciaEnergetica) {
		
		eficienciaEnergetica = Character.toUpperCase(eficienciaEnergetica);
		boolean eficienciaEnergeticaCorrecto;
		
		if(eficienciaEnergetica == 'A'|| eficienciaEnergetica == 'B' || eficienciaEnergetica == 'C' || eficienciaEnergetica == 'D' || eficienciaEnergetica == 'E' || eficienciaEnergetica == 'F') {
			eficienciaEnergeticaCorrecto = true;
		}else {
			eficienciaEnergeticaCorrecto = false;
		}
		
		return eficienciaEnergeticaCorrecto;
		
	}

	public boolean comprobarColor(String color) {
	
		color = color.toUpperCase();
		boolean colorCorrecto;
		
		if(color.equals("BLANCO") || color.equals("ROJO") || color.equals("AZUL") || color.equals("GRIS")) {
			colorCorrecto = true;
		}else {
			colorCorrecto = false;
		}
	
		return colorCorrecto;
	
	}
	
	public double precioFinal(char eficienciaEnergetica, double peso, double precio) {
		
		int pesoInt = (int) peso;
		double precioFinal = precio;
		eficienciaEnergetica = Character.toUpperCase(eficienciaEnergetica);
									
		if (eficienciaEnergetica == 'A') {
			precioFinal += 100;
		}else if(eficienciaEnergetica == 'B') {
			precioFinal += 80;
		}else if(eficienciaEnergetica == 'C') {
			precioFinal += 60;
		}else if(eficienciaEnergetica == 'D') {
			precioFinal += 50;
		}else if(eficienciaEnergetica == 'E') {
			precioFinal += 30;
		}else if(eficienciaEnergetica == 'F') {
			precioFinal += 10;
		}
		
		if (0 <= pesoInt && 19 >= pesoInt) {
			precioFinal += 10;
		}else if(20 <= pesoInt && 49 >= pesoInt) {
			precioFinal += 50;
		}else if(50 <= pesoInt && 79 >= pesoInt) {
			precioFinal += 80;
		}else if(80 < pesoInt) {
			precioFinal += 100;
		}
		
		return precioFinal;
		
	}

	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", eficienciaEnergetica="
				+ eficienciaEnergetica + ", peso=" + peso + "]";
	}
	
}
