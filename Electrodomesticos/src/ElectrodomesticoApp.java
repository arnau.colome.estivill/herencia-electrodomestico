import dto.Electrodomestico;
import dto.Lavadora;
import dto.Television;

import javax.swing.JOptionPane;

public class ElectrodomesticoApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String electrodomesticos[] = new String[100];
		
		Electrodomestico electrodomestico;
		Lavadora lavadora;
		Television television;
		double totaltv = 0, totallavadora = 0, preciototal= 0;
						
		for (int i = 0; i < 10; i++) { 
					
			String[] figura = {"Electrodomestico", "Televisión", "Lavadora"};
			Object seleccion = JOptionPane.showInputDialog(null, "Seleccione uno:", "Seleccion",  JOptionPane.DEFAULT_OPTION, null, figura, "0");
		
			String precioBase = JOptionPane.showInputDialog("Introduce el precio del producto:");
			double precioBaseDouble = Double.parseDouble(precioBase);
			
			String color = JOptionPane.showInputDialog("Introduce el color del producto:");		
			
			String eficienciaEnergetica = JOptionPane.showInputDialog("Introduce el eficiencia energetica del producto:");
			char eficienciaEnergeticaChar = eficienciaEnergetica.charAt(0);
			
			String peso = JOptionPane.showInputDialog("Introduce el peso del producto:");
			double pesoDouble = Double.parseDouble(peso);
						
			if(seleccion.equals("Electrodomestico")) {
				electrodomestico = new Electrodomestico(precioBaseDouble, color, eficienciaEnergeticaChar, pesoDouble);
				
				electrodomesticos[i] = electrodomestico.toString();
			}else if(seleccion.equals("Televisión")) {
				String resolucion = JOptionPane.showInputDialog("Introduce la resolucion:");
				int resolucionInt = Integer.parseInt(resolucion);
				
				int sincronizadorTDT = JOptionPane.showOptionDialog(null, "Tiene sincronizador TDT:", "", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
				boolean sincronizadorTDTBoolean = (sincronizadorTDT == 0) ? true : false;
				
				television = new Television(resolucionInt, sincronizadorTDTBoolean, precioBaseDouble, color, eficienciaEnergeticaChar, pesoDouble);
				electrodomesticos[i] = television.toString();
			}else {
				String carga = JOptionPane.showInputDialog("Introduce la carga:");
				double cargaDouble = Double.parseDouble(carga);

				lavadora = new Lavadora(cargaDouble, precioBaseDouble, color, eficienciaEnergeticaChar, pesoDouble);
				electrodomesticos[i] = lavadora.toString();
			}
			if (seleccion.equals("Televisión")) {
				totaltv = totaltv + precioBaseDouble;
			}
			else if (seleccion.equals("Lavadora")){
				totallavadora = totallavadora + precioBaseDouble;
			}
			preciototal = preciototal + precioBaseDouble;
							
		}
		
		for (int i = 0; i < electrodomesticos.length; i++) {
			System.out.println(electrodomesticos[i]);
		}
				
		System.out.println("Precio total: " + preciototal);
		
		System.out.println("Precio Electrodomesticos: " + (preciototal - (totallavadora+totaltv)));
		System.out.println("Precio Lavadoras: " + totallavadora);
		System.out.println("Precio Televisiones: " + totaltv);
		
	}

}
